import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 153, 15, 13);
  static Color colorPrimary = Color.fromARGB(255, 153, 15, 13);
  static Color colorSecondary = Color.fromARGB(255, 153, 15, 13);
  static Color colorAccent = Color.fromARGB(255, 200, 200, 200);
  static Color colorText = Color.fromARGB(255, 153, 15, 13);
  static Color colorText1 = Color.fromARGB(255, 31, 31, 31);
  static Color colorText2 = Color.fromARGB(255, 185, 185, 185);
}
