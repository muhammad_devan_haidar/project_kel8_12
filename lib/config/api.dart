class Api {
  static const _host = "http://192.168.100.203/api_projekakhir";

  static String _user = "$_host/user";
  static String _databarang = "$_host/data_barang";
  static String _datapengajuan = "$_host/data_pengajuan";
  static String _datapengembalian = "$_host/data_pengembalian";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // barang
  static String addBarang = "$_databarang/add_data_barang.php";
  static String deleteBarang = "$_databarang/delete_data_barang.php";
  static String getBarang = "$_databarang/get_data_barang.php";
  static String updateBarang = "$_databarang/update_data_barang.php";

  // data_pengajuan
  static String addPengajuan = "$_datapengajuan/add_data_pengajuan.php";
  static String deletePengajuan = "$_datapengajuan/delete_data_pengajuan.php";
  static String getPengajuan = "$_datapengajuan/get_data_pengajuan.php";
  static String updatePengajuan = "$_datapengajuan/update_data_pengajuan.php";

  // data_pengajuan
  static String addPengembalian =
      "$_datapengembalian/add_data_pengembalian.php";
  static String deletePengembalian =
      "$_datapengembalian/delete_data_pengembalian.php";
  static String getPengembalian =
      "$_datapengembalian/get_data_pengembalian.php";
  static String updatePengembalian =
      "$_datapengembalian/update_data_pengembalian.php";
}
