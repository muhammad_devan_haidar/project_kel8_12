import 'package:get/get.dart';
import 'package:projek_akhir_kel12/model/pengajuan.dart';

class CPengajuan extends GetxController {
  Rx<Pengajuan> _datapengajuan = Pengajuan().obs;

  Pengajuan get user => _datapengajuan.value;

  void setUser(Pengajuan datadata_pengajuan) =>
      _datapengajuan.value = datadata_pengajuan;
}
