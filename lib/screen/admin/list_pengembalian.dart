import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:projek_akhir_kel12/config/asset.dart';
import 'package:projek_akhir_kel12/event/event_db.dart';
import 'package:projek_akhir_kel12/model/pengembalian.dart';
import 'package:projek_akhir_kel12/screen/admin/add_update_pengembalian.dart';

class ListPengembalian extends StatefulWidget {
  @override
  State<ListPengembalian> createState() => _ListPengembalianState();
}

class _ListPengembalianState extends State<ListPengembalian> {
  List<Pengembalian> _listPengembalian = [];

  void getPengembalian() async {
    _listPengembalian = await EventDb.getPengembalian();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalian();
    super.initState();
  }

  void showOption(Pengembalian? data_pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalian(data_pengembalian: data_pengembalian))
            ?.then((value) => getPengembalian());
        break;
      case 'delete':
        EventDb.deletePengembalian(data_pengembalian!.kodePengembalian!)
            .then((value) => getPengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listPengembalian.length,
                  itemBuilder: (context, index) {
                    Pengembalian pengembalian = _listPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengembalian.kodePengembalian ?? ''),
                      subtitle: Text(pengembalian.tanggalkembali ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdatePengembalian())
                  ?.then((value) => getPengembalian()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
