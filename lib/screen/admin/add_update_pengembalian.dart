import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:projek_akhir_kel12/config/asset.dart';
import 'package:projek_akhir_kel12/event/event_db.dart';
import 'package:projek_akhir_kel12/model/pengembalian.dart';
// import 'package:projek_akhir_kel12/screen/admin/list_pengajuan.dart';
import 'package:projek_akhir_kel12/screen/admin/list_pengembalian.dart';
// import 'package:projek_akhir_kel12/model/barang.dart';
// import 'package:projek_akhir_kel12/screen/admin/list_barang.dart';
import 'package:projek_akhir_kel12/widget/info.dart';
// import 'package:project_kelas/config/asset.dart';
// import 'package:project_kelas/event/event_db.dart';
// import 'package:project_kelas/screen/admin/list_mahasiswa.dart';
// import 'package:project_kelas/widget/info.dart';

// import '../../model/mahasiswa.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? data_pengembalian;
  AddUpdatePengembalian({this.data_pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengembalian = TextEditingController();
  var _controllerkodePengajuan = TextEditingController();
  var _controllertanggalKembali = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengembalian != null) {
      _controllerkodePengembalian.text =
          widget.data_pengembalian!.kodePengembalian!;
      _controllerkodePengajuan.text = widget.data_pengembalian!.kodePengajuan!;
      _controllertanggalKembali.text =
          widget.data_pengembalian!.tanggalkembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengembalian != null
            ? Text('Update Data Pengembalian')
            : Text('Tambah Data Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggalKembali,
                  decoration: InputDecoration(
                      labelText: "Tanggal Kembali",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengembalian == null) {
                        String message = await EventDb.AddPengembalian(
                          _controllerkodePengembalian.text,
                          _controllerkodePengajuan.text,
                          _controllertanggalKembali.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengembalian.clear();
                          _controllerkodePengajuan.clear();
                          _controllertanggalKembali.clear();

                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                          _controllerkodePengembalian.text,
                          _controllerkodePengajuan.text,
                          _controllertanggalKembali.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
