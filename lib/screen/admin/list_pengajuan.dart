import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:projek_akhir_kel12/config/asset.dart';
import 'package:projek_akhir_kel12/event/event_db.dart';
import 'package:projek_akhir_kel12/model/pengajuan.dart';
import 'package:projek_akhir_kel12/screen/admin/add_update_pengajuan.dart';

class ListPengajuan extends StatefulWidget {
  @override
  State<ListPengajuan> createState() => _ListPengajuanState();
}

class _ListPengajuanState extends State<ListPengajuan> {
  List<Pengajuan> _listPengajuan = [];

  void getPengajuan() async {
    _listPengajuan = await EventDb.getPengajuan();

    setState(() {});
  }

  @override
  void initState() {
    getPengajuan();
    super.initState();
  }

  void showOption(Pengajuan? data_pengajuan) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuan(data_pengajuan: data_pengajuan))
            ?.then((value) => getPengajuan());
        break;
      case 'delete':
        EventDb.deletePengajuan(data_pengajuan!.kodePengajuan!)
            .then((value) => getPengajuan());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPengajuan.length > 0
              ? ListView.builder(
                  itemCount: _listPengajuan.length,
                  itemBuilder: (context, index) {
                    Pengajuan pengajuan = _listPengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengajuan.kodePengajuan ?? ''),
                      subtitle: Text(pengajuan.namaPeminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengajuan),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengajuan())?.then((value) => getPengajuan()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
