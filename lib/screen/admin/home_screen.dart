import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:projek_akhir_kel12/config/asset.dart';
import 'package:projek_akhir_kel12/event/event_pref.dart';
import 'package:projek_akhir_kel12/screen/admin/list_barang.dart';
import 'package:projek_akhir_kel12/screen/admin/list_pengajuan.dart';
import 'package:projek_akhir_kel12/screen/admin/list_pengembalian.dart';
import 'package:projek_akhir_kel12/screen/admin/list_user.dart';
import 'package:projek_akhir_kel12/screen/login.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Asset.colorPrimary,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Image(
                image: NetworkImage(
                    'https://teknokrat.ac.id/wp-content/uploads/2023/04/Web-header-UTI-23.jpg')),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(
              'Kategori',
              style: TextStyle(
                  color: Asset.colorText1,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            alignment: Alignment.topLeft,
          ),
          Container(
            padding: EdgeInsets.only(top: 25, bottom: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton('User', Icons.supervised_user_circle, 0),
                    IconButton('Barang', Icons.bookmark_outlined, 1),
                    IconButton('Pengajuan', Icons.arrow_circle_up, 2),
                    IconButton('Pengembalian', Icons.arrow_circle_down, 3),
                    IconButton('Logout', Icons.logout, 99),
                  ],
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Berita Hari Ini',
                  style: TextStyle(
                    color: Asset.colorText1,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Text(
                  'Lihat semua',
                  style: TextStyle(
                    color: Asset.colorPrimary,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            alignment: Alignment.topLeft,
          ),
          SizedBox(
            height: 0,
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              'Rekomendasi berita dari kampus kita tercinta UTI',
              style: TextStyle(
                  color: Asset.colorText2,
                  fontWeight: FontWeight.bold,
                  fontSize: 14),
            ),
            alignment: Alignment.topLeft,
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 140,
            child: ListView(
              children: [
                CarouselSlider(
                  items: [
                    SlideItem(
                        'https://teknokrat.ac.id/wp-content/uploads/2023/04/Sekar-Kinasih-040423-1.jpg'),
                    SlideItem(
                        'https://teknokrat.ac.id/wp-content/uploads/2023/01/WhatsApp-Image-2023-01-04-at-18.00.09.jpeg'),
                    SlideItem(
                        'https://teknokrat.ac.id/wp-content/uploads/2023/04/Tim-Tari-Teknokrat-120323.jpeg'),
                    SlideItem(
                        'https://teknokrat.ac.id/wp-content/uploads/2023/04/ddaa.png'),
                  ],
                  options: CarouselOptions(
                    height: 140.0,
                    enlargeCenterPage: true,
                    autoPlay: true,
                    aspectRatio: 16 / 9,
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enableInfiniteScroll: true,
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    viewportFraction: 0.8,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                'Top Peminjam',
                style: TextStyle(
                    color: Asset.colorText1,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              alignment: Alignment.topLeft),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 235, 207, 207),
              borderRadius: BorderRadius.all(
                Radius.circular(16.0),
              ),
            ),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0), // Atur padding foto di sini
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.asset(
                      'asset/images/woman.jpg',
                      width: 60.0,
                      height: 60.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: 8.0), // Atur padding teks di sini
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Diandra asep',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Sistem Informasi',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black38,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 235, 207, 207),
              borderRadius: BorderRadius.all(
                Radius.circular(16.0),
              ),
            ),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0), // Atur padding foto di sini
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.asset(
                      'asset/images/woman.jpg',
                      width: 60.0,
                      height: 60.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: 8.0), // Atur padding teks di sini
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Eneng Cantik',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Sastra Inggris',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black38,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class SlideItem extends StatelessWidget {
  final String img;

  SlideItem(this.img);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey.shade300)),
      child: Image(
          image: NetworkImage(
            img,
          ),
          fit: BoxFit.cover),
    );
  }
}

class IconButton extends StatelessWidget {
  final String nameLabel;
  final IconData iconLabel;
  final int index;

  IconButton(this.nameLabel, this.iconLabel, this.index);

  List<Map> _fragment = [
    {'title': 'Daftar User', 'view': ListUser()},
    {'title': 'Daftar Barang', 'view': ListBarang()},
    {'title': 'Daftar Pengajuan', 'view': ListPengajuan()},
    {'title': 'Daftar Pengembalian', 'view': ListPengembalian()},
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            // margin: EdgeInsets.only(bottom: 5),
            child: Material(
              borderRadius: BorderRadius.all(
                Radius.circular(40),
              ),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.all(
                  Radius.circular(40),
                ),
                onTap: () {
                  if (index == 99) {
                    EventPref.clear();
                    Get.off(Login());
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => _fragment[index]['view']),
                    );
                  }
                },
                child: Container(
                  // margin: EdgeInsets.all(5),
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    color: Asset.colorPrimaryDark,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  child: Center(
                    child: Stack(
                      children: [
                        Icon(
                          iconLabel,
                          color: Colors.white,
                          size: 40,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Text(
              nameLabel,
              style: TextStyle(fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}
