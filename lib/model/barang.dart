class Barang {
  String? kodeBarang;
  String? namaBarang;
  String? jumlah;

  Barang({
    this.kodeBarang,
    this.namaBarang,
    this.jumlah,
  });

  factory Barang.fromJson(Map<String, dynamic> json) => Barang(
        kodeBarang: json['kode_barang'],
        namaBarang: json['nama_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_barang': this.kodeBarang,
        'nama_barang': this.namaBarang,
        'jumlah': this.jumlah,
      };
}
