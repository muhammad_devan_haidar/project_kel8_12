class Pengembalian {
  String? kodePengembalian;
  String? kodePengajuan;
  String? tanggalkembali;

  Pengembalian({
    this.kodePengembalian,
    this.kodePengajuan,
    this.tanggalkembali,
  });

  factory Pengembalian.fromJson(Map<String, dynamic> json) => Pengembalian(
        kodePengembalian: json['kode_pengembalian'],
        kodePengajuan: json['kode_pengajuan'],
        tanggalkembali: json['tanggal_kembali'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengembalian': this.kodePengembalian,
        'kode_pengajuan': this.kodePengajuan,
        'tanggal_kembali': this.tanggalkembali,
      };
}
